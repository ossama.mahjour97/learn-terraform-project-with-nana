provider "aws" {
  region     = "us-east-2"
  access_key = ""
  secret_key = ""
}

# *************create vp-c *************** 

resource "aws_vpc" "ossama-vpc" {
  cidr_block       = var.vpc_cidr_block 
  tags = {
    Name = "first-vpc"
  }
}





# *************create subnet attach to vp-c *************** 

resource "aws_subnet" "subnet-1" {
  vpc_id     = aws_vpc.ossama-vpc.id
  cidr_block = var.subnet_cidr_block 
  availability_zone= "us-east-2a"
  tags = {
    Name = "prod-subnet"
  }
}

# *************create subnet for an existing vpc using data sources   *************** 
data "aws_vpc" "existing_vpc" {
  default = true 
}

resource "aws_subnet" "subnet-2" {
  vpc_id     = data.aws_vpc.existing_vpc.id
  cidr_block = "172.31.32.0/20" 
  availability_zone= "us-east-2a"
  tags = {
    Name = "subnet using data sources"
  }

}

# ************* output   ***************  

#output "aws_subnet" {
 # value= aws_subnet.subnet-1.id
#}

#output "aws_output_vpc" {
 # value= aws_vpc.ossama-vpc.id
#}


# ************* using variables   ***************  
 variable "subnet_cidr_block" {
   description= "subnet cidr block" 
 }


 variable "vpc_cidr_block" {
   description= "subnet cidr block" 
 }

